<?php

namespace springdev\yii2\oauth2server\storage;

class MongoDB extends MongoNewDriver {

    public $connection = 'mongodb';

    public function __construct($connection = null, $config = array()) {
        if ($connection === null) {
            if (!empty($this->connection)) {
                $connection = \Yii::$app->get($this->connection);
                $connection = $connection->dsn;
            } else {
               throw new \InvalidArgumentException('Please set the Database connection name to {mongodb}');
            }
        }

        parent::__construct($connection, $config);
    }

}
