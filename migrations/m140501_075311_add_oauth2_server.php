<?php

use yii\db\Schema;

class m140501_075311_add_oauth2_server extends \yii\mongodb\Migration {

    public function up() {
        $this->createCollection('oauth_clients', [
            'client_id' => Schema::TYPE_STRING,
            'client_secret' => Schema::TYPE_STRING,
            'redirect_uri' => Schema::TYPE_STRING,
            'grant_types' => Schema::TYPE_STRING,
            'scope' => Schema::TYPE_STRING,
            'user_id' => Schema::TYPE_INTEGER
        ]);

        $this->createCollection('oauth_access_tokens', [
            'access_token' => Schema::TYPE_STRING,
            'client_id' => Schema::TYPE_STRING,
            'user_id' => Schema::TYPE_INTEGER,
            'expires' => Schema::TYPE_TIMESTAMP,
            'scope' => Schema::TYPE_STRING
        ]);

        $this->createCollection('oauth_refresh_tokens', [
            'refresh_token' => Schema::TYPE_STRING,
            'client_id' => Schema::TYPE_STRING,
            'user_id' => Schema::TYPE_INTEGER,
            'expires' => Schema::TYPE_TIMESTAMP,
            'scope' => Schema::TYPE_STRING,
        ]);

        $this->createCollection('oauth_authorization_codes', [
            'authorization_code' => Schema::TYPE_STRING,
            'client_id' => Schema::TYPE_STRING,
            'user_id' => Schema::TYPE_INTEGER,
            'redirect_uri' => Schema::TYPE_STRING ,
            'expires' => Schema::TYPE_TIMESTAMP,
            'scope' => Schema::TYPE_STRING
        ]);

        $this->createCollection('oauth_scopes', [
            'scope' => Schema::TYPE_STRING,
            'is_default' => Schema::TYPE_BOOLEAN,
        ]);

        $this->createCollection('oauth_jwt', [
            'client_id' => Schema::TYPE_STRING,
            'subject' => Schema::TYPE_STRING,
            'public_key' => Schema::TYPE_STRING,
        ]);

        $this->createCollection('oauth_users', [
            'username' => Schema::TYPE_STRING,
            'password' => Schema::TYPE_STRING,
            'first_name' => Schema::TYPE_STRING,
            'last_name' => Schema::TYPE_STRING,
        ]);

        $this->createCollection('oauth_public_keys', [
            'client_id' => Schema::TYPE_STRING,
            'public_key' => Schema::TYPE_STRING,
            'private_key' => Schema::TYPE_STRING,
            'encryption_algorithm' => Schema::TYPE_STRING,
        ]);

        // insert client data
        $this->insert('oauth_clients', [
            'client_id'=>'testclient',
            'client_secret'=>'testpass', 
            'redirect_uri'=>'http://fake/', 
            'grant_types'=>'client_credentials authorization_code password implicit'
            ]);
    }

    public function down() {
        $this->dropCollection('oauth_users');
        $this->dropCollection('oauth_jwt');
        $this->dropCollection('oauth_scopes');
        $this->dropCollection('oauth_authorization_codes');
        $this->dropCollection('oauth_refresh_tokens');
        $this->dropCollection('oauth_access_tokens');
        $this->dropCollection('oauth_public_keys');
        $this->dropCollection('oauth_clients');
    }

}
