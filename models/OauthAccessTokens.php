<?php

namespace springdev\yii2\oauth2server\models;

use Yii;
use yii\mongodb\ActiveRecord;

/**
 * This is the model class for table "oauth_access_tokens".
 *
 * @property string $access_token
 * @property string $client_id
 * @property integer $user_id
 * @property string $expires
 * @property string $scope
 *
 * @property OauthClients $client
 */
class OauthAccessTokens extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function collectionName() {
        return 'oauth_access_tokens';
    }

    public function attributes() {
        return ['_id', 'access_token', 'user_id', 'expires', 'access_token', 'client_id', 'scope'];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['access_token', 'client_id', 'expires'], 'required'],
            [['expires'], 'safe'],
            [['access_token'], 'string', 'max' => 40],
            [['client_id'], 'string', 'max' => 32],
            [['scope'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'access_token' => 'Access Token',
            'client_id' => 'Client ID',
            'user_id' => 'User ID',
            'expires' => 'Expires',
            'scope' => 'Scope',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient() {
        return $this->hasOne(OauthClients::className(), ['client_id' => 'client_id']);
    }

}
