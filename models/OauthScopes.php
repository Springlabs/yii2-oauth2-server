<?php

namespace springdev\yii2\oauth2server\models;

use Yii;
use yii\mongodb\ActiveRecord;
/**
 * This is the model class for table "oauth_scopes".
 *
 * @property string $scope
 * @property integer $is_default
 */
class OauthScopes extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function collectionName() {
        return 'oauth_scopes';
    }

    public function attributes() {
        return ['_id', 'scope', 'is_default'];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['scope', 'is_default'], 'required'],
            [['is_default'], 'integer'],
            [['scope'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'scope' => 'Scope',
            'is_default' => 'Is Default',
        ];
    }

}
